import java.util.Scanner;
 
public class Rechner_Aufgabe2  
{ 
   
  public static void main(String[] args)
  { 
     
	//Aufgabe 2 
	//Erstellen  Sie  ein  neues  Programm.  Begr��en  Sie  den  Nutzer  und  fragen  ihn  nach  seinem 
	//Namen  und  seinem  Alter.  Speichern  Sie  beide  Eingaben  in  Variablen  und  geben  diese  am 
	//Ende des Programms in der Konsole aus.

	Scanner myScanner2 = new Scanner(System.in); 
	System.out.print("Guten Tag! Wie hei�en Sie?: "); 
	
	String Name = myScanner2.nextLine();  
	System.out.print("Und wie alt sind Sie?: "); 
	
	int age = myScanner2.nextInt();   
	
	System.out.print("Prima! Sie sind ");  
	System.out.print(Name + " und Sie sind " + age + " Jahre alt");  
	myScanner2.close();
     
  }    
}