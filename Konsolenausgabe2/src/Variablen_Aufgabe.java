public class Variablen_Aufgabe {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	 short zaehler;
    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  zaehler=25;
	  System.out.println("Zaehlerstand = "+zaehler);
    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
	  String menuepunkt;
    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  menuepunkt = "C";
	  System.out.println(menuepunkt);
    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
	  double grosszahl;
    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
	  grosszahl = 299792458;
	  System.out.println("Grosse Zahl = "+grosszahl+" m/s");

    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	  short mitgliederZahl = 7;
    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	  System.out.println("Anzahl der Mitglieder: "+mitgliederZahl);
    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
	  final double elementar_ladung = 1.602E-19;
	  System.out.println(elementar_ladung);
    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	  boolean istBezahlt;
    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
	  istBezahlt= true; 
	  	
	  	System.out.println(istBezahlt);
  }//main
}// Variablen