
import java.util.Scanner; // Import der Klasse Scanner 
 
public class Rechner_Aufgabe1  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
	  //Aufgabe 1 
	  //Geben  Sie  das  folgend  dargestellte  Programm  in  ihrer  Entwicklungsumgebung  ein  und 
	  //vollziehen den Quellcode f�r sich nach.  
	  //Erweitern Sie das Programm so, dass die weiteren Grundrechenarten ausgef�hrt werden und 
	  //die jeweiligen Ergebnisse untereinander ausgegeben werden.
	  
	  
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    int zahl1 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    int zahl2 = myScanner.nextInt();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    double ergebnis1 = zahl1 + zahl2;
    double ergebnis2 = zahl1 - zahl2;
    double ergebnis3 = zahl1 * zahl2;
    double ergebnis4 = zahl1 / zahl2;

    System.out.print("\n\n\nErgebnis der Addition lautet: ");
    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis1);
    
    System.out.print("\n\n\nErgebnis der Addition lautet: ");   
    System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis2);
    
    System.out.print("\n\n\nErgebnis der Addition lautet: ");  
    System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis3);
    
    System.out.print("\n\n\nErgebnis der Addition lautet: ");  
    System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnis4);
    
    myScanner.close(); 
     
  }    
}