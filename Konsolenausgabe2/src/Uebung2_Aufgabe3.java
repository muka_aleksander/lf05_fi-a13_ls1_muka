
public class Uebung2_Aufgabe3 {
public static void main (String[] args) {
	double c1,c2,c3,c4,c5;
    int a,b,c,d,e;
    a=-20;
    b=-10;
    c=+0;
    d=+20;
    e=+30;
    c1=(a-32)/1.8;
    c2=(b-32)/1.8;
    c3=(c-32)/1.8;
    c4=(d-32)/1.8;
    c5=(e-32)/1.8;
    System.out.printf("%-10s |", "Fahrenheit ");
    System.out.printf("%12s  %n", "Celsius ");
    System.out.println("------------------------");
    System.out.printf( "%-12s|", a );
    System.out.printf( "%10.2f \n", c1 );
    System.out.printf( "%-12s|", b );
    System.out.printf( "%10.2f\n", c2 );
    System.out.printf( "%-12s|", c );
    System.out.printf( "%10.2f\n", c3 );
    System.out.printf( "%-12s|", d );
    System.out.printf( "%10.2f\n", c4 );
    System.out.printf( "%-12s|", e );
    System.out.printf( "%10.2f\n", c5 );
}
}