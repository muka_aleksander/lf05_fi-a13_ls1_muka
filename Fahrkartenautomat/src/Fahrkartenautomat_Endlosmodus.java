import java.util.Scanner;

class Fahrkartenautomat_Endlosmodus
{
    public static void main(String[] args)
    {
    	while (true) {
    		
    		
    		
    		
	    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
	    	double r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	    	fahrkartenAusgeben();
	    	rueckgeldAusgeben(r�ckgabebetrag);
	
	    	System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.\n\n"+
	                          "--------------------------------------------------\n\n");
    	}
    }
    
    
    //_________________________________________________________________
    
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);

        double ticketEinzelpreis = 1.0;
        boolean running = false;

        
        
        
        do {
        	System.out.print("Bitte w�hlen Sie eine der folgenden Ticketarten:\n\n (1) Kurzstrecke Regeltarif AB [2,00 �] \n (2) Einzelfahrschein Regeltarif AB [3,00 �] \n (3) 24-Stunden-Karte Regeltarif AB [8,80 �] \n");
        	int auswahl = tastatur.nextInt();
        	
        	switch (auswahl) {
            case 1: ticketEinzelpreis = 2.0; running = false; break;
            case 2: ticketEinzelpreis = 3.0; running = false; break;
            case 3: ticketEinzelpreis = 8.8; running = false; break;
            default: 
            	System.out.println("\nGeben Sie eine Ganzzahl zwischen 1 und 3 ein.\n");
            	running = true;
            	break;
            }
        	
        	
        } while (running);
        
        System.out.print("Wie viele Tickets werden gekauft?: ");
        double anzahlTickets = tastatur.nextInt();
    	
    	return ticketEinzelpreis * anzahlTickets;
    }
    
    
    //__________________________________
    
    
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneM�nze;
        
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),"  ");
     	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	
    	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void warte (int time) {
    	try {
  			Thread.sleep(time);
  		} catch (InterruptedException e) {
  			e.printStackTrace();
  		}
    }
    
    
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrscheine werden ausgegeben.");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    
    
    public static void muenzeAusgeben(double r�ckgabebetrag, String einheit) {
    	
    	while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
    	
        {
     	  System.out.println("2 " + einheit);
	          r�ckgabebetrag -= 2.0;
	          
        }
        while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
        {
     	  System.out.println("1 " + einheit);
	          r�ckgabebetrag -= 1.0;
	          
        }
        while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
        {
     	  System.out.println("0.50 " + einheit);
	          r�ckgabebetrag -= 0.5;
	          
        }
       
        while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
        {
     	  System.out.println("0.20 " + einheit);
	          r�ckgabebetrag -= 0.2;
	          
        }
        while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
        {
     	  System.out.println("0.10 " + einheit);
	          r�ckgabebetrag -= 0.1;
	          
        }
        while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
        {
     	  System.out.println("0.05 " + einheit);
	          r�ckgabebetrag -= 0.05;
	          
        }
    }
    
    
    //____________________________________________
    
    
    public static void rueckgeldAusgeben(double r�ckgabebetrag) {
    	if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO", r�ckgabebetrag);
     	   System.out.println("\nwird in folgenden M�nzen ausgezahlt:");
     	   muenzeAusgeben(r�ckgabebetrag, " EURO");
        }
    }
}