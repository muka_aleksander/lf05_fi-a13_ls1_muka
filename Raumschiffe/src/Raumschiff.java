import java.util.*;
public class Raumschiff {
	
	//	Attribute
	
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;
	

	
	//	Konstruktor
	
	public Raumschiff() {
		ladungsverzeichnis = new ArrayList<>();
	}
	public Raumschiff(int photonentorpedoAnzahl, 
			int energieversorgungInProzent,
			int schildeInProzent, 
			int huelleInProzent,
			int lebenserhaltungssystemeInProzent,
			int androidenAnzahl,
			String schiffsname) {
	}
	//	Verwaltungsmethoden



	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setSchiffsname(String Schiffsname) {
		this.schiffsname = Schiffsname;
	}
	
	public String getSchiffsname() {
		return this.schiffsname;
	}
	
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	//Methoden
	
	
	public void addLadung(Ladung neueLadung){
		ladungsverzeichnis.add(neueLadung);
	}
	public void addComm(String SM){
		broadcastKommunikator.add(SM);
	}
	public void photonentorpedoSchiessen(int photonentorpedoAnzahl, Raumschiff r){
		if (photonentorpedoAnzahl == 0) {
			System.out.println("-=*Click*=-");
		}
		
		//TREFFER SZENARIO SCHILDE 50+
		else if (r.getSchildeInProzent() >= 50){
		photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
		System.out.println("Photonentorpedo abgeschossen");
		r.schildeInProzent = r.schildeInProzent - 50;
		System.out.println("Treffer!" + "\n" + r.getSchiffsname() + " Schilde sind auf: " + r.getSchildeInProzent() + "%");
		}
		
		//TREFFER SZENARIO SCHILDE 49-
		else if (r.getSchildeInProzent() < 50){
			photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
			System.out.println("Photonentorpedo abgeschossen");
			r.schildeInProzent = 0;
			r.huelleInProzent = r.huelleInProzent - 50;
			r.energieversorgungInProzent = r.energieversorgungInProzent - 50;
			System.out.println("Treffer!" + "\n" + r.getSchiffsname() + " Schilde sind auf: " + r.getSchildeInProzent() + "%");
			System.out.println("\n" + "H�lle ist auf: " + r.getHuelleInProzent() + "%");
			if (r.getHuelleInProzent() == 0) {
				System.out.println("\nZIEL ZERST�RT!");
				}
			else {
				
			}
		}
			
		
	}
		
	
	public void phaserkanoneSchiessen(int energieversorgungInProzent, Raumschiff r){
		if (energieversorgungInProzent <= 50) {
			System.out.println("-=*Click*=-");
		}
		//TREFFER SZENARIO SCHILDE 50+
		else if (r.getSchildeInProzent() >= 50){
		energieversorgungInProzent = energieversorgungInProzent - 50;
		System.out.println("Phaserkanone abgeschossen");
		r.schildeInProzent = r.schildeInProzent - 50;
		System.out.println("Treffer!" + "\n" + r.getSchiffsname() + " Schilde sind auf: " + r.getSchildeInProzent() + "%");
		}
		
		//TREFFER SZENARIO SCHILDE 49-
		else if (r.getSchildeInProzent() < 50){
			energieversorgungInProzent = energieversorgungInProzent - 50;
			System.out.println("Phaserkanone abgeschossen");
			r.schildeInProzent = 0;
			r.huelleInProzent = r.huelleInProzent - 50;
			r.energieversorgungInProzent = r.energieversorgungInProzent - 50;
			System.out.println("\n" + "Treffer!" + r.getSchiffsname() + " Schilde sind auf: " + r.getSchildeInProzent() + "%");
			System.out.println("\n" + "H�lle ist auf: " + r.getHuelleInProzent() + "%");
			if (r.getHuelleInProzent() == 0) {
				System.out.println("\nZIEL ZERST�RT!");
				}
			else {
				
			}
		}
	}
	

	//toString
	
	@Override
	public String toString() {
		return  "Schiffsname: " + schiffsname + 
				"\nEnergieversorgung: " + energieversorgungInProzent + "%" +
				"\nSchilde: " + schildeInProzent + "%" +
				"\nHuelle: " + huelleInProzent +  "%" +
				"\nLebenserhaltungssysteme: " + lebenserhaltungssystemeInProzent + "%" +
				"\nPhotonentorpedo: " + photonentorpedoAnzahl + 
				"\nAndroiden: " + androidenAnzahl;
				//"\nbroadcastKommunikator=" + Arrays.toString(broadcastKommunikator);
	}
}
		





	