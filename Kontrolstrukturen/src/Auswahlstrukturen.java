import java.util.Scanner;

public class Auswahlstrukturen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("   BMI \n");

		System.out.println("Klassifikation |   m   |  w");
		
		
		System.out.println("------------------------------");
		
		
		System.out.println("Untergewicht   | <20   | <19");
		System.out.println("Normalgewicht  | 20-25 | 19-24");
		System.out.println("Übergewicht    | >25   | >24\n");

		Scanner tastatur = new Scanner(System.in);
//
		char Geschlecht;
		double Körpergewicht;
		double Körpergröße;
//
		System.out.println("Wie groß sind Sie? (in cm): ");
		Körpergröße = tastatur.nextDouble();

		System.out.println("Wie viel wiegen Sie? (in Kg): ");
		Körpergewicht = tastatur.nextDouble();

		System.out.println("Dein Geschlecht ist: w oder m");
		Geschlecht = tastatur.next().charAt(0);
		
		//_____________

		double BMI = Körpergewicht / ((Körpergröße / 100) * (Körpergröße / 100));
		System.out.println("Dein BMI ist: " + BMI);

		if ((Geschlecht == 'm' && BMI < 20) || (Geschlecht == 'w' && BMI < 19)) {
			System.out.println("Es ist - Untergewicht");
			
			
			//
			
		} else if ((Geschlecht == 'm' && BMI >= 20 && BMI <= 25) || (Geschlecht == 'w' && BMI >= 19 && BMI <= 24)) {
			System.out.println("Es ist - Normalgewicht");
			
		} else if ((Geschlecht == 'm' && BMI > 25) || (Geschlecht == 'w' && BMI > 24)) {
			System.out.println("Es ist - Übergewicht");
		}

	}
}